
const readme = 'README.md';
const mdPath = `./static/${readme}`;
const logPath = './logs/';
const defaultLogFile = 'access.log';
const propertiesFile = './conf/properties.yml';
const downloadServePath = '/ms-download';

exports.readme = readme;
exports.mdPath = mdPath;
exports.logPath = logPath;
exports.defaultLogFile = defaultLogFile;
exports.propertiesFile = propertiesFile;
exports.downloadServePath = downloadServePath;