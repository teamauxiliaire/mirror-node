# A Node.js Mirror Server #

##### A minimal mirroring server written in Node.js (Express) #####

The server accepts requests, and mirrors (echoes) them back. Ideal to mock third party services.

### Features ###

#### Backend ####

* Support for the following methods:

    * GET
    * POST
    * PUT
    * PATCH
    * DELETE
    
* Supports arbitrary paths (but GET /client, /static, /ms-download)
* Captures query parameters
* Captures JSON body
* Captures all headers
* CORS support
* Supports logging of the requests into file
* Supports logging arbitrary events by adding a predefined `msLogStringParameter` parameter to the request body
* Override default configuration with configuration file (sample in `/conf/properties.default.yml`)
* Command line parameters (add these after `node start -- `):

    * __--log[=<logfile>], -l[=<logfile>]:__ log requests to specified file (default: properties.yml:mirrorServer.logger.logFile or access.log)
	* __--debug, -d:__ verbose output to provide information about what the app does in the background

#### Frontend ####

This is but a mirror server on steroids. It comes with a nice admin dashboard:

![Admin screenshot](https://bytebucket.org/teamauxiliaire/mirror-node/raw/58c1f7bf537a91a64646ea178507f5912a7490af/static/images/mirror-server-client-dashboard.small.png?token=b947e8ad9dc226b7dab3351bed8a9b24d6d19dd7 "A screenshot of the admin page")

* Admin Dashboard to conveniently follow the requests: [/client](http://127.0.0.1:3000/client)
* Save sessions to easily switch between sets of requests
* Filter requests by full-text search
* Reset log at any time
* Expand headers for details
* Download available logs with one click
* Verbose console logging if `debug` js variable set to `true` (set in console: `let debug = true`)

### Requirements ###

```commandline
npm install
```

### Start the server ###

```commandline
npm start
```

Debug / Verbose output:

```commandline
npm start -- --debug
```

### Supported requests ###

* GET (returns '0'):

	* paths
	* queries
   
* POST, PUT, PATH, DELETE:

	* multipart/form-data
	* application/json
	* application/x-www-form-urlencoded

### Usage ###

* Send one of the supported requests to [http://127.0.0.1:3000/](http://127.0.0.1:3000/) with any content you'd like to get back as response
* Check the standard output or the client [Dashboard](http://127.0.0.1:3000/client) to follow the incoming requests.
* send arbitrary strings directly into the log using the `msLogStringParameter` parameter in the request body

### Configuration ###

Configuration is done by creating a `properties.yml` file in the `conf` folder. A sample structure is found there in `properties.default.yml`. It looks like the following:

```yaml
mirrorServer:
    app:
        # Provide detailed console output (default: false):
        # verbose: true
    logger:
        # Override the default log file (access.log) with this line:
        logFile: write.log
        # Set a custom log file for inputStringAppender (default: logs into <logFile>):
        inputLogFile: input.log
        # Use this list to override default log appenders [ consoleAppender, sockjsAppender ]:
        appenders:
            # Write logs to console:
            - consoleAppender
            # Write logs to predefined log file:
            - fileAppender
            # Write logs to SockJS client:
            - sockjsAppender
            # Add support to incoming log requests:
            - inputStringAppender
```
