const compression = require('compression');
const express = require('express');
const cors = require('cors')
const app = express();
const bodyParser = require('body-parser');
const multer = require('multer'); // v1.0.5
const upload = multer(); // for parsing multipart/form-data
const c = require('./lib/colors');
const sockjs = require('sockjs');
const path = require('path');
const serverStatic = require('serve-static');
const utils = require('./lib/utils');
const logger = require('./lib/logger');
const constants = require('./conf/constants');
const connMan = require('./lib/connectionManager');
const appManager = require('./lib/appManager');

appManager.bootstrap();

app.set('view engine', 'pug');

app.use(cors());
app.use(compression());
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use('/static', express.static(path.join(__dirname, 'static')));

// Reserved paths:
// - reserved for client
app.get('/client', function (req, res) {
    res.render('client');
});
// - reserved for downloads
app.get(new RegExp(`^\\${constants.downloadServePath}\/(\\w+(\\.\\w+)?)$`), function (req, res) {
    const file = path.join(constants.logPath, req.params[0]);
    res.download(file); // Set disposition and send it.
});

let sockjs_opts = { sockjs_url: "http://cdn.jsdelivr.net/sockjs/1.0.1/sockjs.min.js" };

let sockjs_server = sockjs.createServer(sockjs_opts);
sockjs_server.on('connection', function (conn) {
    connMan.setSockJsConnection(conn);
});

app.get('/*', (req, res) => {
    logger.logRequest(req);
    res.send(req.query.token ? '1' : '0');
});
app.post('/*', upload.array(), (req, res, next) => {
    logger.logRequest(req);
    res.status(201).json(req.body);
});
app.put('/*', upload.array(), (req, res, next) => {
    logger.logRequest(req);
    res.json(req.body);
});
app.patch('/*', upload.array(), (req, res, next) => {
    logger.logRequest(req);
    res.json(req.body);
});
app.delete('/*', upload.array(), (req, res, next) => {
    logger.logRequest(req);
    res.json(req.body);
});

const server = app.listen(3000, () => console.log(c.embed(c.bright + c.fgGreen) + ' ' + c.embed(c.dim + c.fgWhite), 'Inspector app listening on port 3000', `(pid: ${process.pid})`));

sockjs_server.installHandlers(server, { prefix: '/sockjs' });
