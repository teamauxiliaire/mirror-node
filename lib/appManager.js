const connMan = require('./connectionManager');
const utils = require('./utils');
const constants = require('../conf/constants');
const fs = require('fs');
const yaml = require('js-yaml');

const fetchReadmeCommand = 'fetchReadme';
const rootKey = 'mirrorServer';
const appKey = 'app';
const appVerboseKey = 'verbose';
const loggerKey = 'logger';
const loggerLogFileKey = 'logFile';
const loggerInputLogFileKey = 'inputLogFile';
const loggerAppendersKey = 'appenders';
const pushReadmeEventType = 'readme';


let initHandlers = [];

let properties = {
    app: {
        verbose: false
    },
    logger: {
        logPath: constants.logPath,
        logFile: constants.defaultLogFile
    }
};

const bootstrap = function () {
    fetchProperties();
    fetchArgs();
    prepareMd();
    setSigHandlers();

    initHandlers.forEach(function (func) {
        func();
    });
};

const logHandler = function (value) {
    if (value) {
        properties.logger.logFile = value;
    }
    utils.primary(`Logging enabled using file '${properties.logger.logFile}'`);
};

const debugHandler = function (value) {
    properties.app.verbose = true;
};

const argHandlers = {
    '--log': logHandler,
    '-l': logHandler,
    '--debug': debugHandler,
    '-d': debugHandler
};

const addInitHandler = function (initHandler) {
    initHandlers.push(initHandler);
};

const fetchArgs = function () {
    let args = process.argv.slice(2);
    args.forEach(function (val, i, arr) {
        let parts = val.split('=');
        let opt = parts[0];
        val = parts[1];
        if (argHandlers.hasOwnProperty(opt)) {
            argHandlers[opt](val);
        }
    });
};

const prepareMd = function () {
    fs.writeFile(constants.mdPath, '', function (err) {
        if (err) throw err;
        fs.createReadStream(constants.readme).pipe(fs.createWriteStream(constants.mdPath));
        utils.log(`${constants.readme} prepared`);
    });
};

const cleanup = function () {
    fs.unlink(constants.mdPath, function (err) {
        utils.success('Cleanup done.');
        process.exit(0);
    });
};

const setSigHandlers = function () {
    process.on('SIGINT', cleanup);
    process.on('SIGTERM', function () {
        server.close(function () {
            process.exit(0);
        });
    });
};

const fetchProperties = function () {
    let found = [];
    try {
        const doc = yaml.safeLoad(fs.readFileSync(constants.propertiesFile, 'utf8'));
        if (doc && doc.hasOwnProperty(rootKey)) {
            const root = doc[rootKey];
            if (root && root.hasOwnProperty(appKey)) {
                const rootApp = root[appKey];
                if (rootApp) {
                    if (rootApp.hasOwnProperty(appVerboseKey) && typeof rootApp[appVerboseKey] === 'boolean') {
                        properties.app.verbose = rootApp[appVerboseKey];
                    }
                }
            }
            if (root && root.hasOwnProperty(loggerKey)) {
                const rootLogger = root[loggerKey];
                if (rootLogger) {
                    if (rootLogger.hasOwnProperty(loggerLogFileKey) && rootLogger[loggerLogFileKey]) {
                        properties.logger.logFile = properties.logger.logFile === constants.defaultLogFile ? rootLogger[loggerLogFileKey] : properties.logger.logFile;
                        found.push(loggerLogFileKey);
                    }
                    if (rootLogger.hasOwnProperty(loggerInputLogFileKey) && rootLogger[loggerInputLogFileKey]) {
                        properties.logger.inputLogFile = rootLogger[loggerInputLogFileKey];
                        found.push(loggerInputLogFileKey);
                    }
                    if (rootLogger.hasOwnProperty(loggerAppendersKey) && Array.isArray(rootLogger[loggerAppendersKey])) {
                        properties.logger.appenders = rootLogger[loggerAppendersKey];
                        found.push(loggerAppendersKey);
                    }
                }
            }
        }
        utils.info('Properties file found.');
        if (found.length > 0) {
            utils.info(`Fetched properties: ${found}, updated properties:`);
        } else {
            utils.info('No properties set, using defaults:');
        }
        utils.log(properties);
    } catch (e) {
        utils.warn(`Could not read properties file (${e})`);
    }
};

const fetchReadmeHandler = function () {
    fs.readFile(constants.readme, 'utf8', (err, data) => {
        if (err) throw err;
        utils.info(`Sending ${constants.readme}`);
        connMan.writeToSock({ content: data }, pushReadmeEventType);
    });
};

addInitHandler(function () { connMan.registerHandler(fetchReadmeCommand, fetchReadmeHandler); });

exports.properties = properties;
exports.bootstrap = bootstrap;
exports.addInitHandler = addInitHandler;