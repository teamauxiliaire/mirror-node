const utils = require('./utils');

const commandKey = 'command';

let sockjs_conn = null;
let commandHandlers = new Object();

const writeToSock = function (data, eventType) {
    if (typeof sockjs_conn !== 'undefined' && sockjs_conn !== null) {
        data['eventType'] = eventType;
        sockjs_conn.write(JSON.stringify(data));
    } else {
        utils.warn('SockJS connection is not available!');
    }
};

const setSockJsConnection = function (conn) {
    sockjs_conn = conn;
    sockjs_conn.on('data', function (message) {
        message = JSON.parse(message);
        if (message.hasOwnProperty(commandKey)) {
            utils.info(`Command (${message[commandKey]}) received`);
            if (commandHandlers.hasOwnProperty(message[commandKey])) {
                utils.info('Command found');
                commandHandlers[message[commandKey]](sockjs_conn);
            }
        }
    });
};

const registerHandler = function (command, handler) {
    utils.info(`Handler for command (${command}) registered`);
    commandHandlers[command] = handler;
};

exports.writeToSock = writeToSock;
exports.setSockJsConnection = setSockJsConnection;
exports.registerHandler = registerHandler;