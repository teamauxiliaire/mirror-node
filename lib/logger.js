const c = require('./colors');
const fs = require('fs');
const utils = require('./utils');
const moment = require('moment');
const constants = require('../conf/constants');
const appMan = require('./appManager');
const connMan = require('./connectionManager');
const winston = require('winston');

const consoleAppender = 'consoleAppender';
const fileAppender = 'fileAppender';
const sockjsAppender = 'sockjsAppender';
const inputStringAppender = 'inputStringAppender';

const inputStringKey = 'msLogStringParameter';

const defaultStorageHint = 'logFile';
const inputStorageHint = 'inputLogFile';

const requestReceivedEventType = 'request';
const logCreatedEventType = 'logCreated';

const fetchLogsCommand = 'fetchAvailableLogs';

let availableLogs = [];

const appenderImpl = {
    consoleAppender: function (pack) {
        let methodColor = c.bright + c.fgMagenta;
        switch (pack.method) {
            case 'GET':
                methodColor = c.bright + c.fgGreen;
                break;
            case 'POST':
                methodColor = c.bright + c.fgYellow;
                break;
            case 'PUT':
                methodColor = c.bright + c.fgCyan;
                break;
            case 'PATCH':
                methodColor = c.bright + c.fgWhite;
                break;
            case 'DELETE':
                methodColor = c.bright + c.fgRed;
                break;
        }
        //console.log(c.embed(methodColor) + ' ' + c.embed(c.bright + c.fgMagenta), utils.pad(pack.method, 6), pack.fullUrl);
        //console.log(pack.body);
        winston.log('info', c.embed(methodColor) + ' ' + c.embed(c.bright + c.fgMagenta), utils.pad(pack.method, 6), pack.fullUrl);
        winston.log('info', pack.body);
    },
    fileAppender: function (pack) {
        let method = utils.pad(pack.method, 6);
        fileLog(`${moment().format('YYYY-MM-DD HH:mm:ss.SSS')} [ ${method} ] ${pack.fullUrl}`);
    },
    sockjsAppender: function (pack) {
        connMan.writeToSock(pack, requestReceivedEventType);
    },
    inputStringAppender: function (pack) {
        if (Object.prototype.hasOwnProperty.call(pack.body, inputStringKey) && pack.body[inputStringKey]) {
            fileLog(pack.body[inputStringKey], inputStorageHint);
        }
    }
};

let appenders = null;
let lineIndex = 0;

const getAppenders = function () {
    if (!appenders) {
        appenders = Array.isArray(appMan.properties.logger.appenders) ? appMan.properties.logger.appenders : [
            consoleAppender,
            sockjsAppender
        ];
        utils.info('Appenders enabled:');
        utils.log(appenders);
    }
    return appenders;
};

const logRequest = function (req) {
    lineIndex++;

    const pack = packify(req);

    utils.keys(appenderImpl).filter(function (key) {
        return getAppenders().indexOf(key) !== -1;
    }).forEach(function (key) {
        appenderImpl[key](pack);
    });
};

const packify = function (req) {

    const fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
    const baseUrl = req.protocol + '://' + req.get('host') + req.path;

    return {
        'requestIndex': lineIndex,
        'body': req.body,
        'method': req.method,
        'originalUrl': req.originalUrl,
        'fullUrl': fullUrl,
        'baseUrl': baseUrl,
        'path': req.path,
        'headers': utils.toList(req.headers),
        'params': req.query
    };
};

const prepareLogNotification = function (logFile, mention = true) {
    if (mention) {
        utils.info(`Sending 1 new available log`);
    }
    return { logFile: logFile, logUrl: `${constants.downloadServePath}/${logFile}` };
};

const fileLog = function (line, storageHint = defaultStorageHint) {
    let logFile;
    if (appMan.properties.logger.hasOwnProperty(storageHint)) {
        logFile = appMan.properties.logger[storageHint];
    } else {
        logFile = appMan.properties.logger[defaultStorageHint];
    }
    fs.appendFile(appMan.properties.logger.logPath + logFile, line + "\n", (err) => {
        if (err) throw err;
        if (availableLogs.indexOf(logFile) === -1) {
            availableLogs.push(logFile);
            connMan.writeToSock(prepareLogNotification(logFile), logCreatedEventType);
        }
    });
};

appMan.addInitHandler(function () {
    connMan.registerHandler(fetchLogsCommand, function () {
        fs.readdir(constants.logPath, function (err, files) {
            if (err) throw err;
            availableLogs = [];
            const logs = files.filter(function (val) {
                return !val.startsWith('.');
            });
            const len = logs.length;
            utils.info(`Sending ${len} available ${len === 1 ? 'log' : 'logs'}`);
            logs.forEach(function (file) {
                availableLogs.push(file);
                connMan.writeToSock(prepareLogNotification(file, false), logCreatedEventType);
            });
        });
    });
});

exports.consoleAppender = consoleAppender;
exports.fileAppender = fileAppender;
exports.sockjsAppender = sockjsAppender;

exports.defaultStorageHint = defaultStorageHint;
exports.inputStorageHint = inputStorageHint;

exports.lineIndex = lineIndex;
exports.logRequest = logRequest;
exports.fileLog = fileLog;