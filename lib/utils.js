const c = require('./colors');
const util = require('util');
const constants = require('../conf/constants');
const appMan = require('./appManager');
const connMan = require('./connectionManager');

const padAlignLeft = 'padAlignLeft';
const padAlignRight = 'padAlignRight';

const keys = function (obj) {
    let keys = [];
    for (k in obj) {
        if (obj.hasOwnProperty(k))
            keys.push(k);
    }
    return keys;
};

const toList = function (obj) {
    let list = [];
    for (let i in obj) {
        if (obj.hasOwnProperty(i)) {
            list.push([i, obj[i]]);
        }
    }
    return list;
};

const log = function (line, ...opt) {
    if (appMan.properties.app.verbose) {
        if (opt.length > 0)
            console.log(line, opt);
        else
            console.log(line);
    }
};

const warn = function (line) {
    log(c.embed(c.dim + c.fgYellow), 'Warn: ' + line);
};

const primary = function (line) {
    log(c.embed(c.bright + c.fgBlue), line);
};

const success = function (line) {
    log(c.embed(c.bright + c.fgGreen), line);
};

const error = function (line) {
    log(c.embed(c.bright + c.fgRed), 'Error: ' + line);
};

const info = function (line) {
    log(c.embed(c.dim + c.fgGreen), line);
};

const pad = function (str, len, chr = ' ', align = padAlignLeft) {
    switch (align) {
        case padAlignLeft:
            return str + chr.repeat(len).substring(0, 6 - str.length);
        case padAlignRight:
            return chr.repeat(len).substring(0, 6 - str.length) + str;
        default:
            warn(`pad - invalid align (${align})`);
            return str;
    }
};

exports.keys = keys;
exports.toList = toList;
exports.log = log;
exports.warn = warn;
exports.primary = primary;
exports.success = success;
exports.error = error;
exports.info = info;
exports.pad = pad;