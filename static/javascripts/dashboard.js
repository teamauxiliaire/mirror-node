
var sockjs_url = '/sockjs';
var sockjs = new SockJS(sockjs_url);

var DashboardModel = function () {
    var self = this;

    // Logs
    self.availableLogs = ko.observableArray();
    self.addAvailableLog = function (logFile) {
        if (logFile !== '') {
            self.availableLogs.push(logFile);
        }
    }.bind(self);
    self.fetchLogs = function (item) {
        if (typeof item === 'undefined' || !item.hasOwnProperty('logUrl') || item.logUrl === '#') {
            self.availableLogs.removeAll();
            sockjs.send(JSON.stringify({ command: 'fetchAvailableLogs' }));
            return false;
        } else {
            return true;
        }
    }.bind(self);
    self.getAvailableLogs = ko.pureComputed(function() {
        if (self.availableLogs().length > 0) {
            return self.availableLogs().sort();
        } else {
            return [{ logFile: 'Non found. Refresh?', logUrl: '#' } ];
        }
    }, self);

    // Requests
    self.lines = ko.observableArray();
    self.tmpLines = [];
    self.addLine = function (line) {
        if (line !== '') {
            self.lines.unshift(line);
        }
    }.bind(self);
    self.index = ko.computed(function () {
        return self.lines().length + 1;
    }, self);
    self.searchPhrase = ko.observable('');

    // Readme handling
    self.aboutPageMarkdown = ko.observable('');
    self.getAboutPage = ko.pureComputed(function () {
        var converter = new showdown.Converter();
        return converter.makeHtml(self.aboutPageMarkdown());
    }, self);
    self.fetchReadme = function () {
        sockjs.send(JSON.stringify({ command: 'fetchReadme' }));
    };

    // Util
    self.extractLines = function (selfLines) {
        var lines = [];
        selfLines.forEach(function (item, index) {
            lines[index] = item;
        });
        return lines;
    };
    self.filterLines = function (self, event) {
        var searchPhrase = event.target.value;
        if (searchPhrase !== '') {
            if (self.tmpLines.length === 0) {
                self.tmpLines = self.extractLines(self.lines());
            }
            if (event.keyCode === 8) { // Backspace
                self.lines(self.tmpLines.filter(function (line) {
                    var patt = new RegExp(searchPhrase, 'i');
                    return patt.test(line.raw);
                }));
            } else {
                self.lines(self.lines().filter(function (line) {
                    var patt = new RegExp(searchPhrase, 'i');
                    return patt.test(line.raw);
                }));
            }
        } else if (self.tmpLines.length > 0) {
            self.lines(self.tmpLines);
            self.tmpLines = [];
        }
    };
    self.resetLines = function () {
        self.searchPhrase('');
        self.lines([]);
        self.tmpLines = [];
    }.bind(self);

    // Sessions
    self.isNewSession = ko.observable(false);
    self.sessionName = ko.observable('');
    self.sessions = ko.observableArray();
    self.activeSession = ko.observable('');
    self.newSession = function () {
        self.isNewSession(true);
        $('#sessionNameInput').focus();
    }.bind(self);
    self.createSessionByEnter = function (data, event) {
        if (event.which === 13) {
            self.createSession();
        }
    }.bind(self);
    self.createSession = function () {
        if (self.sessionName() !== '') {
            self.sessions.unshift({
                "name": self.sessionName(),
                "lines": self.extractLines(self.lines())
            });
            self.isNewSession(false);
            self.sessionName('');
            feather.replace();
        }
    }.bind(self);
    self.restoreSession = function (session) {
        self.lines(session.lines);
    }.bind(self);
    self.deleteSession = function (session) {
        self.sessions.remove(session);
    }.bind(self);

    // Routing
    self.dashboardPage = ko.observable('dashboard');
    self.aboutPage = ko.observable();

    self.goToDashboard = function () { location.hash = ''; };
    self.goToAbout = function () { location.hash = 'about'; };

    Sammy(function () {
        this.get('#:page', function () {
            if (this.params.page === 'dashboard') {
                self.aboutPage(null);
                self.dashboardPage('dashboard');
            } else {
                self.dashboardPage(null);
                self.aboutPage('about');
            }
        });

        this.get('', function () { this.app.runRoute('get', '#dashboard'); });
    }).run();

    // Visibility fading animation handler:
    ko.bindingHandlers.fadeVisible = {
        init: function (element, valueAccessor) {
            var shouldDisplay = valueAccessor();
            $(element).toggle(shouldDisplay);
        },
        update: function (element, valueAccessor) {
            var shouldDisplay = valueAccessor();
            shouldDisplay ? $(element).fadeIn() : $(element).hide();
        }
    };
};
var model = new DashboardModel();

sockjs.onopen = function () {
    console.log('[*] open', sockjs.protocol);
    // Fetching available logs:
    model.fetchLogs();
    // Fetching Readme:
    model.fetchReadme();
};
sockjs.onmessage = function (e) {
    if (typeof debug !== 'undefined' && debug)
        console.dir(e.data);
    var data = JSON.parse(e.data);
    if (data.hasOwnProperty('eventType')) {
        if (data.eventType === 'request') {
            data['index'] = model.index();
            data['raw'] = e.data;
            model.addLine(data);
        } if (data.eventType === 'logCreated' && data.hasOwnProperty('logFile')) {
            if (model.availableLogs().indexOf(data) === -1) {
                model.addAvailableLog(data);
                if (typeof feather !== 'undefined')
                    feather.replace();
            }
        } else if (data.eventType === 'readme' && data.hasOwnProperty('content')) {
            model.aboutPageMarkdown(data.content);
        }
    }
};

ko.applyBindings(model);
